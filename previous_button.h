#ifndef PREVIOUS_BUTTON_H
#define PREVIOUS_BUTTON_H

#include <QPushButton>

class PreviousButton : public QPushButton {
    Q_OBJECT
private:
public:
    PreviousButton();

    int heightForWidth(int w) const { return w; }

    void resizeEvent(QResizeEvent *event);

public slots:
    void clicked();

signals:
    void previousVideo();
};

#endif // PREVIOUS_BUTTON_H