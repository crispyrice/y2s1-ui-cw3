#include "next_button.h"

#include <QPushButton>
#include <QResizeEvent>

NextButton::NextButton() : QPushButton() {

    setMaximumSize(QSize(50, 50));
    setMinimumSize(QSize(30, 30));
    setIcon(QIcon(":/icons/next.svg"));
    connect(this, SIGNAL(released()), this, SLOT(clicked()));
}

void NextButton::resizeEvent(QResizeEvent* event) {
    //shrink to smaller dimension
    int target;
    if (width()>height())
        target=height();
    else
        target=width();

    //keep button square
    resize(target, target);
    setIconSize(QSize(target*.8, target*.8));
}

void NextButton::clicked() {
    emit nextVideo();
}