#include "volume_control.h"
#include <QAudio>

#define CONSTANT_PADDING (3)
void VolumeControl::updateIcon(){
    if(muted)
       button->setIcon(*muteIcon);
    else
       button->setIcon(*unmuteIcon);
}
void VolumeControl::changeValue(int val){

    if(val>0){
        //non zero volume, need to unmute
        lastVolume=val;
        muted=false;
    }
    else
        muted=true;

    //reflect state change
    updateIcon();
    //volume scales logarithmically for better UX
    qreal convertedVolume=QAudio::convertVolume(val/qreal(100.0),QAudio::LogarithmicVolumeScale,
                                                QAudio::LinearVolumeScale);
    emit changeVolume(convertedVolume*100);
    emit changeMute(muted);
}
void VolumeControl::toggleMute(){
    if(muted)
        slider->setValue(lastVolume);//restore last volume
    else
        slider->setValue(0);//mute
}
void VolumeControl::resizeEvent(QResizeEvent *event){
    //shrink to smaller dimension
    int target=height();

    //keep button square
    button->resize(target,target);
    button->setIconSize(QSize(target,target));

    resize(target*5,target);

}
