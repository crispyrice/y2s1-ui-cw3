//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <QMediaPlayer>
#include <QUrl>

#include <iostream>

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

void ThePlayer::shuffleVideos() {
    shuffle(begin(*infos), end(*infos), rng);

    for (uint i = 0; i < buttons->size(); i++) {
        buttons->at(i)->init(&infos->at(i));
    }
}

void ThePlayer::goToNextVideo() {
    TheButtonInfo* nextVideo = Q_NULLPTR;

    for (uint i = 0; i < infos->size(); i++) {
        TheButtonInfo btn = infos->at(i);

        if (currentVideo->url->matches(*btn.url, QUrl::None)) {
            if (i + 1 == infos->size())
                nextVideo = buttons->at(0)->info;
            else nextVideo = buttons->at(i + 1)->info;
            break;
        }
    }

    if (nextVideo == Q_NULLPTR) return;

    jumpTo(nextVideo);
}

void ThePlayer::goToPreviousVideo() {
    TheButtonInfo* prevVideo = Q_NULLPTR;

    for (uint i = 0; i < infos->size(); i++) {
        TheButtonInfo btn = infos->at(i);

        if (currentVideo->url->matches(*btn.url, QUrl::None)) {
            if (i == 0)
                prevVideo = buttons->at(buttons->size() - 1)->info;
            else prevVideo = buttons->at(i - 1)->info;
            break;
        }
    }

    if (prevVideo == Q_NULLPTR) return;

    jumpTo(prevVideo);
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    currentVideo = button;
    setMedia( * button -> url);
    play();
    emit videoChanged();
}
void ThePlayer::togglePlayback(){
    if(state()!=QMediaPlayer::PlayingState)
        play();
    else
        pause();
}
