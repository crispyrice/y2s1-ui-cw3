#ifndef PAUSE_BUTTON_H
#define PAUSE_BUTTON_H

#include <QPushButton>
class PauseButton:public QPushButton{
    Q_OBJECT

public:
    PauseButton();
    void resizeEvent(QResizeEvent *event);
private:
    bool playState=true;
    //play and pause icons
    QIcon plIcon, paIcon;
public slots:
    void resetState();
private slots:
    void clicked();

signals:
    void togglePlayback();
};

#endif // PAUSE_BUTTON_H
