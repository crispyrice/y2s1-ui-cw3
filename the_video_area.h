#ifndef THE_VIDEO_AREA_H
#define THE_VIDEO_AREA_H

#include "the_player.h"
#include "pause_button.h"
#include "volume_control.h"
#include "the_videoslider.h"
#include "the_title.h"
#include "fullscreen_button.h"
#include "menubar.h"
#include "shuffle_button.h"
#include "next_button.h"
#include "previous_button.h"

#include <QWidget>
#include <QVideoWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
/**
 * @brief Video area widget contains the video and player controls
 */
class TheVideoArea : public QWidget {
private:
    QVideoWidget* videoWidget;
    ThePlayer* player;
    PauseButton* pauseButton;
    VolumeControl* volControl;
    TheVideoslider* videoSlider = NULL;
    FullscreenButton* fullscreenButton;
    Menubar* menuBar;
    ShuffleButton* shuffleButton;
    NextButton* nextButton;
    PreviousButton* prevButton;
    the_title* title = new the_title();

    //used to hide external objects for fullscreen mode
    QWidget* externalHide = NULL;

    // TODO: Move these classes to their own file
    QWidget* controlContainer = NULL;
    QVBoxLayout* l;
public:
    TheVideoArea(QWidget& window);
    //~TheVideoArea();
    QVideoWidget* getWidget();
    ThePlayer* getPlayer();
    the_title* getTitle();

    void setExternalHide(QWidget* widget);
    //reimplemented to adjust control size
    void setGeometry(int ax, int ay, int aw, int ah);
};

#endif
