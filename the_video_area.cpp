#include "the_video_area.h"
#include <QShortcut>
TheVideoArea::TheVideoArea(QWidget& window) {
    videoWidget = new QVideoWidget;
    player = new ThePlayer;

    player->setVideoOutput(videoWidget);

    // play/pause widget
    pauseButton = new PauseButton();
    // setup signals and slots
    pauseButton->connect(pauseButton, SIGNAL(togglePlayback()), player, SLOT(togglePlayback()));
    player->connect(player, SIGNAL(videoChanged()), pauseButton, SLOT(resetState()));

    // Setup next and previous buttons
    nextButton = new NextButton();
    nextButton->connect(nextButton, SIGNAL(nextVideo()), player, SLOT(goToNextVideo()));
    prevButton = new PreviousButton();
    prevButton->connect(prevButton, SIGNAL(previousVideo()), player, SLOT(goToPreviousVideo()));

    // volume control
    volControl= new VolumeControl();
    volControl->connect(volControl, SIGNAL(changeVolume(int)), player, SLOT(setVolume(int)));
    volControl->connect(volControl, SIGNAL(changeMute(bool)), player, SLOT(setMuted(bool)));
    
    // shuffle button
    shuffleButton = new ShuffleButton();
    shuffleButton->connect(shuffleButton, SIGNAL(shuffleVideos()), player, SLOT(shuffleVideos()));

    //fullscreen button
    fullscreenButton= new FullscreenButton();
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOn()), &window, SLOT(showFullScreen()));
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOff()), &window, SLOT(showNormal()));

    //Hotkey
    new QShortcut(QKeySequence(Qt::Key_F11), fullscreenButton, SLOT(clicked()));

    // add pause button, volume control and fullscreen
    QHBoxLayout *controlRow= new QHBoxLayout();
    //left group
    controlRow->addWidget(volControl, 0, Qt::AlignLeft);

    //center group
    QHBoxLayout *centerLayout = new QHBoxLayout();
    centerLayout->addWidget(prevButton);
    centerLayout->addWidget(pauseButton);
    centerLayout->addWidget(nextButton);
    QWidget *centerContainer= new QWidget();
    centerContainer->setLayout(centerLayout);
    centerLayout->setMargin(0);//line up with other widgets
    controlRow->addWidget(centerContainer,0,Qt::AlignHCenter);

    //right group
    QHBoxLayout *rightLayout = new QHBoxLayout();
    rightLayout->addWidget(shuffleButton);
    rightLayout->addWidget(fullscreenButton);
    QWidget *rightContainer = new QWidget();
    rightContainer->setLayout(rightLayout);
    rightLayout->setMargin(0);//line up with other widgets
    controlRow->addWidget(rightContainer, 0, Qt::AlignRight);
    controlRow->setSpacing(0);

    controlContainer= new QWidget();
    controlContainer->setLayout(controlRow);


    // Add video slider
    videoSlider = new TheVideoslider();
    videoSlider->connect(videoSlider, SIGNAL(changeTime(qint64)), player, SLOT(setPosition(qint64)));
    videoSlider->connect(player, SIGNAL(positionChanged(qint64)), videoSlider, SLOT(resetTimeline()));
    videoSlider->connect(player, SIGNAL(videoChanged()), videoSlider, SLOT(resetForSameVideo()));
    videoSlider->connect(player, SIGNAL(durationChanged(qint64)), videoSlider, SLOT(videoDurationChange(qint64)));
    videoSlider->connect(player, SIGNAL(positionChanged(qint64)), videoSlider, SLOT(videoPositionChange(qint64)));
    videoSlider->connect(player, SIGNAL(videoChanged()), videoSlider, SLOT(resetTimeline()));

    //Add menubar and connect openNewVideo signal to jumpTo in the_player
    menuBar = new Menubar();
    menuBar->connect(menuBar, SIGNAL(openNewVideoURL(TheButtonInfo*)), player, SLOT(jumpTo(TheButtonInfo*)));

    title = new the_title();


    //additional hides
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOn()), menuBar, SLOT(hide()));
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOff()), menuBar, SLOT(show()));
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOn()), title, SLOT(hide()));
    fullscreenButton->connect(fullscreenButton, SIGNAL(fullscreenOff()), title, SLOT(show()));

    //todo - mouse event for controlContainer


    //setup size policies
    videoWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    //add components to layout
    l = new QVBoxLayout();
    l->addWidget(videoWidget);
    l->addWidget(controlContainer);
    l->addWidget(videoSlider);
    l->addWidget(menuBar);
    l->addWidget(title);
    l->setSpacing(0);
    l->setMargin(0);
    setLayout(l);
}
//boundary for using the smallest buttons
#define VID_AREA_SHORT (400)


void TheVideoArea::setGeometry(int ax, int ay, int aw, int ah){
    QWidget::setGeometry(ax, ay, aw, ah);
    int targetSize=50;

    //increase if necessary, maxes out at 600p
    if(ah>VID_AREA_SHORT){
        if(ah-VID_AREA_SHORT<200)
            targetSize+=20*((ah-VID_AREA_SHORT)%200)/200.0;//intermediate value
        else
            targetSize+=20;//maximum
    }

    //not enough horizontal space, get smaller value
    if(aw<15*(targetSize-20))
        targetSize=aw/15+20;

    controlContainer->setFixedHeight(targetSize);
}
void TheVideoArea::setExternalHide(QWidget *widget){
    if(externalHide!=NULL){
        //disconnect old widget, if it exists
        disconnect(fullscreenButton, SIGNAL(fullscreenOn()), externalHide, SLOT(hide()));
        disconnect(fullscreenButton, SIGNAL(fullscreenOff()), externalHide, SLOT(show()));
    }
    externalHide=widget;
    connect(fullscreenButton, SIGNAL(fullscreenOn()), externalHide, SLOT(hide()));
    connect(fullscreenButton, SIGNAL(fullscreenOff()), externalHide, SLOT(show()));
}

QVideoWidget* TheVideoArea::getWidget() { return videoWidget; }
ThePlayer* TheVideoArea::getPlayer() { return player; }

the_title* TheVideoArea::getTitle() { return title; }
