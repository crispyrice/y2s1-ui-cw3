#include "the_responsive_layout.h"

#define WIDTH_THRESHOLD     900
#define HEIGHT_THRESHOLD    500

#define MARGIN_TOP      5
#define MARGIN_BOTTOM   5
#define MARGIN_LEFT     5
#define MARGIN_RIGHT    5

#define RATIO 1.5

using namespace std;

/**
 * @brief Helper enum for getting the layout type
 * @property is_tall If the viewport exceeds @c HEIGHT_THRESHOLD
 * @property is_short If the viewport doesn't exceed @c HEIGHT_THRESHOLD
 * @property is_wide If the viewport exceeds @c WIDTH_THRESHOLD
 * @property is_thin If the viewport doesn't exceed @c WIDTH_THRESHOLD
 */
typedef struct {
    bool is_tall;
    bool is_short;
    bool is_wide;
    bool is_thin;
} LayoutType;

TheResponsiveLayout::TheResponsiveLayout(vector<TheButtonInfo>& videos, QWidget& window) {
    // Video area contains the video widget and the player
    videoArea = new TheVideoArea(window);
    ThePlayer* player = videoArea->getPlayer();

    ThePlaylist playlist = ThePlaylist(videos);
    playlistArea = new ThePlaylistArea(playlist, player, videoArea->getTitle());

    videoArea->setExternalHide(playlistArea);

    addWidget(videoArea);
    addWidget(playlistArea);
}

TheResponsiveLayout::~TheResponsiveLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}

void TheResponsiveLayout::setGeometry(const QRect& r) {
    QLayout::setGeometry(r);

    // Precalculate the layouts to save performance (& probably memory too)
    LayoutType ltype;
    ltype.is_tall   = (r.height() >= HEIGHT_THRESHOLD);
    ltype.is_short  = (r.height() < HEIGHT_THRESHOLD);
    ltype.is_wide   = (r.width() >= WIDTH_THRESHOLD);
    ltype.is_thin   = (r.width() < WIDTH_THRESHOLD);

    // fullscreen - only show player
    // todo - definitive fullscreen check
    if (playlistArea->isHidden()){
        videoArea->setGeometry(
                    r.x() + MARGIN_LEFT,
                    r.y() + MARGIN_TOP,
                    r.width() - (MARGIN_LEFT + MARGIN_RIGHT),
                    r.height() - (MARGIN_TOP + MARGIN_BOTTOM)
        );
        return;
    }

    // Thin layout
    if (ltype.is_thin) {
        videoArea->setGeometry(
            r.x() + MARGIN_LEFT,
            r.y() + MARGIN_TOP,
            r.width() - (MARGIN_LEFT + MARGIN_RIGHT),
            static_cast<int>((r.height() - MARGIN_LEFT - MARGIN_RIGHT) / RATIO)
        );

        playlistArea->setGeometry(
            r.x() + MARGIN_LEFT,
            videoArea->y() + videoArea->height() + MARGIN_TOP,
            r.width() - (MARGIN_LEFT + MARGIN_RIGHT),
            r.height() - videoArea->height()
        );
    }
    else {
        videoArea->setGeometry(
            r.x() + MARGIN_LEFT,
            r.y() + MARGIN_TOP,
            r.width() - 350 - MARGIN_LEFT - MARGIN_RIGHT,
            static_cast<int>(r.height() - MARGIN_LEFT - MARGIN_RIGHT)
        );
        playlistArea->setGeometry(
            videoArea->x() + videoArea->width() + MARGIN_LEFT,
            r.y() + MARGIN_TOP,
            350 - MARGIN_LEFT - MARGIN_RIGHT,
            r.height() - MARGIN_TOP - MARGIN_BOTTOM
        );
    }
}

// following methods provide a trivial list-based implementation of the QLayout class
int TheResponsiveLayout::count() const {
    return list_.size();
}

QLayoutItem *TheResponsiveLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *TheResponsiveLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void TheResponsiveLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}

QSize TheResponsiveLayout::sizeHint() const {
    return minimumSize();
}

QSize TheResponsiveLayout::minimumSize() const {
    return QSize(320, 320);
}
