#ifndef THE_PLAYLIST_AREA_H
#define THE_PLAYLIST_AREA_H

#include "the_playlist.h"
#include "the_player.h"
#include "the_title.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QHBoxLayout>

class ThePlaylistArea : public QWidget {
private:
    ThePlaylist* currentPlaylist;
    QVBoxLayout* vLayout;
    QScrollArea* scrollArea;
    QWidget* buttonWidget;
    QVBoxLayout* buttonLayout;
    vector<TheButton*> buttons;
    vector<the_title*> button_titles;
    vector<QHBoxLayout*> buttonSubLayout;

public:
    /**
     * @brief Construct a new Playlist Area with a current playlist
     * 
     * @param currentPlaylist The playlist to be currently loaded
     * @param player The @c ThePlayer object
     * @param title The @c title object
     */
    ThePlaylistArea(ThePlaylist currentPlaylist, ThePlayer* player, the_title* title);
    /**
     * @brief Delete the playlist area and everything it contains
     */
    ~ThePlaylistArea();

    /**
     * @brief Gets a pointer to the current playlist object
     */
    ThePlaylist* getCurrentPlaylist();

    vector<TheButton*>& getButtons();

    /**
     * @brief Delete the current playlist and replace it with a new one
     */
    void setCurrentPlaylist(const ThePlaylist& playlist);

protected:
    void createWidgets();
};

#endif
