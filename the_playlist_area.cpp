#include "the_playlist_area.h"
#include "the_playlist.h"
#include "the_player.h"

#include <QtDebug>

#include <algorithm>

using namespace std;

// Contains helper
#define CONTAINS(vec, item) (find(vec.begin(), vec.end(), item) != vec.end())

ThePlaylistArea::ThePlaylistArea(ThePlaylist p, ThePlayer* player, the_title* title) {
    // Call the copy constructor for ThePlaylist
    currentPlaylist = new ThePlaylist(p);
    // Create the QLayout
    vLayout = new QVBoxLayout();

    // Physical buttons vector
    buttons = vector<TheButton*>();

    //Physical button titles vector
    button_titles = vector<the_title*>();
    //Create button horizontal subLayout
    buttonSubLayout = vector<QHBoxLayout*>();
    //buttonSubLayoutWidget = vector<QWidget*>();

    // Set the layout for the widget
    setLayout(vLayout);

    // For the buttons and the scrollable area
    scrollArea = new QScrollArea();
    buttonWidget = new QWidget();
    buttonLayout = new QVBoxLayout();

    // Populate the buttons
    for (uint i = 0; i < currentPlaylist->getVideoInfo().size(); i++) {
        TheButton *button = new TheButton(this);
        the_title *button_title = new the_title(); //creates button title object
        QHBoxLayout *subLayout = new QHBoxLayout();

        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), player, SLOT(jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), title, SLOT(jumpTo(TheButtonInfo*))); //when clicked changes player title.

        buttons.push_back(button); //adds element to end of vector
        button_titles.push_back(button_title);
        buttonSubLayout.push_back(subLayout);

        buttonSubLayout[i]->addWidget(button); //adds widget to subLayout
        buttonSubLayout[i]->addWidget(button_title);


        buttonLayout->addLayout(buttonSubLayout[i]);
        button->init(&currentPlaylist->getVideoInfo().at(i));
        button_title->jumpTo(&currentPlaylist->getVideoInfo().at(i));
    }

    buttonWidget->setLayout(buttonLayout);
    scrollArea->setWidget(buttonWidget);
    vLayout->addWidget(scrollArea);

    title->set_the_title(&buttons, &currentPlaylist->getVideoInfo());

    // tell the player what buttons and videos are available
    player->setContent(&buttons, &currentPlaylist->getVideoInfo());
}

ThePlaylistArea::~ThePlaylistArea() {
    // Remove each individual TheButton object from the layout
    for (int i = 0; i < vLayout->count(); i++)
        vLayout->takeAt(i);
        
    delete vLayout;
    // Delete the playlist
    delete currentPlaylist;
}

vector<TheButton*>& ThePlaylistArea::getButtons() {
    return buttons;
}
