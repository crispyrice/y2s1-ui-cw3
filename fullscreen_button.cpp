#include "fullscreen_button.h"
void FullscreenButton::clicked(){
    //toggle fullscreen in model
    screenState=!screenState;
    if(screenState)
        emit fullscreenOn();
    else
        emit fullscreenOff();
}
void FullscreenButton::resizeEvent(QResizeEvent *event){

    //shrink to smaller dimension
    int target;
    if(width()>height())
        target=height();
    else
        target=width();

    //keep button square
    resize(target,target);
    setIconSize(QSize(target*.8,target*.8));
}
