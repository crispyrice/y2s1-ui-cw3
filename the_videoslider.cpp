#include <QtWidgets>
#include "the_videoslider.h"
#include <QString>


TheVideoslider::TheVideoslider()
{
    createWidgets();
    arrangeWidgets();
    makeConnections();
}

void TheVideoslider::createWidgets() {
    label1 = new QLabel("start");
    //QString durationString = QString::number(duration);
    label2 = new QLabel("end");
    slider = new QSlider(Qt::Horizontal, this);
}

//arranges widgets into a horizontal layout, like most video players.
void TheVideoslider::arrangeWidgets() {
    QHBoxLayout *layout = new QHBoxLayout();

    layout->addWidget(label1);
    layout->addWidget(slider);
    layout->addWidget(label2);

    setLayout(layout);
}

//connects the slider position with a slot to detect when the user moves the slider.
void TheVideoslider::makeConnections() {
    connect(slider, SIGNAL(sliderMoved(int)), this, SLOT(vSliderMoved(int)));
}

//emits a signal with the new slider position.
void TheVideoslider::vSliderMoved(int time) {
    emit changeTime(static_cast<qint64>(time*1000));
}

//a slot for changing the range of the slider and the duration label, when the video changes.
void TheVideoslider::videoDurationChange(qint64 time) {
    slider->setRange(0, time/1000);
    slider->setValue(0);
    slider->setTickInterval(time/1000);
    label2->setNum(static_cast<int>(time/1000));
}

//a slot for altering the position label for when the user moves the slider,
//or for normal video playback progression.
void TheVideoslider::videoPositionChange(qint64 time) {
    label1->setNum(static_cast<int>(time/1000));
    slider->setValue(slider->value()+1);
}

//loops the slider when the current video ends and loops.
void TheVideoslider::resetTimeline() {
    if (slider->value() == slider->maximum()) {
        slider->setValue(0);
    }
}

//resets timeline for when the same video is clicked again.
void TheVideoslider::resetForSameVideo() {
    slider->setValue(0);
    slider->setSliderPosition(0);
}
